## Config files templater

Package provides templating of [mako-based](http://www.makotemplates.org/) templates of package config files. 


### Purpose
The main idea is, that config can be generated via console arguments of it`s required values. It is useful for deploy with configuration systems like ansible and others. 

There is no more reason to keep config template itself separately from application (in deploy scripts/roles/whatever) or to define example with comments what config values require changing. 

Besides that, package or program, as usual, is situated in system package directory (with config templates), that looks like one more problem during configuring.


### Installation (standalone usage)
```bash
sudo -H python 3 -m pip install conf-templater
```


### Usage (standalone)
```bash
conf_templater --help
```
or
```bash
config_templater --help
```


### Usage (python package)
- Add `conf-templater` to *requirements.txt*.

- Create cli script file, for example `mypackage/scripts/config.py`, that contains:

```python
from conf_templater import template_config


def run():
    template_config(package='mypackage', subpath='config')


if __name__ == '__main__':
    run()
```

- Create config file in package structure, for example `mypackage/config/development.mako`

- Define entry point in `setup.py` file:
```python
from setuptools import setup

setup(
    name='mypackage',
    install_requires=[
        'conf_templater'
    ],
    entry_points={
        'console_scripts': [
            'mypackage_config = mypackage.scripts.config:run'
        ]
    }
)

```

- Run for development
```bash 
python3 -m pip install -e .
```

- Run for help
```bash
mypackage_config --help
```
