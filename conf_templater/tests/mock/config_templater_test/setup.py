from setuptools import setup

setup(
    name='config_templater_test',
    version='0.1.0',
    packages=['config_templater_test'],
    url='',
    license='MIT',
    author='Vladimir V. Pivovarov',
    author_email='admin@p-app.ru',
    description='Templater test'
)
