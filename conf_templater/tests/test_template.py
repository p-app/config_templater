import os
import tempfile
from subprocess import run


def test_templating(python, test_package_path, config_templater):
    run(
        (
            python,
            '-m',
            'pip',
            'install',
            '-e',
            test_package_path,
        ),
        check=True,
    )

    temp = tempfile.mkstemp()[1]
    run(
        (
            config_templater,
            '--package',
            'config_templater_test',
            '--subpath',
            'conf',
            '--name',
            'test',
            '--output',
            temp,
            '--output-is-a-file',
            'a=1',
            'b=2',
        ),
        check=True,
    )

    with open(temp, 'r', encoding='utf-8') as file:
        assert file.read() == '[test]\na = 1\nb = 2'

    tempdir = tempfile.mkdtemp()
    run(
        (
            config_templater,
            '--package',
            'config_templater_test',
            '--subpath',
            'conf',
            '--type',  # backward compatibility
            'test',
            '--output',
            tempdir,
            '--output-name',
            'test-2',
            '--output-ext',
            'txt',
            'a=1',
            'b=2'
        ),
        check=True,
    )

    assert os.path.exists(os.path.join(
        tempdir, 'test-2.txt'
    )) is True
