import os
import sys

import pytest

CURRENT_DIRPATH = os.path.dirname(os.path.abspath(__file__))


@pytest.fixture()
def python():
    bin_dirpath = os.path.dirname(sys.executable)
    return os.path.join(bin_dirpath, 'python')


@pytest.fixture()
def config_templater():
    bin_dirpath = os.path.dirname(sys.executable)
    return os.path.join(bin_dirpath, 'conf_templater')


@pytest.fixture()
def test_package_path():
    return os.path.join(CURRENT_DIRPATH, 'mock', 'config_templater_test')
